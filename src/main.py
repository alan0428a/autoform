from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import pathlib
import os
import time
import configparser
import logging
    

def main():
    logging.basicConfig(filename='log.txt', level=logging.INFO)
    working_path = os.path.dirname(pathlib.Path().resolve())
    src_path = os.path.join(working_path, 'src')
    print(src_path)

    config = configparser.ConfigParser()
    config.read(os.path.join(src_path, "config.ini"))

    driver_path = os.path.join(working_path, 'ChromeDriver', 'chromedriver.exe')
    logging.info('Chrome Driver loaded at %s', driver_path)

    driver = webdriver.Chrome(driver_path)

    url = config.get("Account", "url")
    driver.get(url)
    logging.info('Heading %s', url)


    def find_element_until(tag, type, timeout=15):
        return WebDriverWait(driver, timeout).until(
            EC.presence_of_element_located((type, tag))
        )

    # 1. Sign-in
    try:
        username_box = find_element_until('loginfmt', By.NAME)
        username_box.send_keys(config.get("Account", "user"))

        logging_button = find_element_until('idSIButton9', By.ID)
        logging_button.click()

        time.sleep(2)

        password_box = find_element_until('passwd', By.NAME)
        password_box.send_keys(config.get("Account", "password"))

        logging_button = find_element_until('idSIButton9', By.ID)
        logging_button.click()    

        time.sleep(2)

        # Wait until the Remember Me page is shown

        logging_button = find_element_until('idBtn_Back', By.ID)
        logging_button.click()

        # 2. Fill in the form    

        health_selection = find_element_until('r3e66d18b1c5d4260b9f6f3abffaac66e', By.NAME)
        health_selection.click()

        # emoji_selection =  driver.find_element_by_xpath("//li[@id='kx']")
        # emoji_selection.click()

        time.sleep(3) # Let the user actually see something!
        logging.info("Sucess " + str(time.time()))
    except Exception as e:
        logging.error(e)
        logging.error("Failed " + str(time.time()))
    finally:
        driver.quit()
  

if __name__ == '__main__':
    main()